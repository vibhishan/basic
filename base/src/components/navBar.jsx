import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';


class NavBar extends Component {
    // state = {  }
    render() { 
        return (
          <div className="container-fluid nav_bg">
            <div className='row'>
              <div className="col-12">
                  <nav className="navbar navbar-expand-lg navbar-light bg-light">
                     <div className="container-fluid">
          <NavLink 
            className="navbar-brand" 
            to="#"
            >
              Navbar
        </NavLink>
          <button 
              className="navbar-toggler" 
              type="button" 
              data-toggle="collapse" 
              data-target="#navbarSupportedContent" 
              aria-controls="navbarSupportedContent" 
              aria-expanded="false" 
              aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>

          <div 
              className="collapse navbar-collapse" 
              id="navbarSupportedContent"
              >
            <ul className="navbar-nav ml-auto mb-2 mb-lg-0">
              <li className="nav-item">
                <NavLink 
                  exact
                  activeClassName='menu_active' 
                  className="nav-link active" 
                  aria-current="page" 
                  to="/"
                  >
                    Home
                  
                 </NavLink>
              </li>
              <li className="nav-item">
                <NavLink 
                  activeClassName='menu_active' 
                  className="nav-link" 
                  to="/services"
                  >
                    Service
                   </NavLink>
              </li>
              <li className="nav-item">
                <NavLink 
                  activeClassName='menu_active' 
                  className="nav-link" 
                  to="/about"
                  >
                    About
                   </NavLink>
              </li>
              <li className="nav-item">
                <NavLink 
                  activeClassName='menu_active' 
                  className="nav-link" 
                  to="/contact"
                  >
                    Contact
                   </NavLink>
              </li>
              
             
            </ul>
            
          </div>
        </div>
      </nav> 
      </div>
            </div>
            </div>
            );
    }
}
 
export default NavBar;