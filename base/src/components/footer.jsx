import React, { Component } from 'react';

const Footer = () => {
    return ( 
        <footer className='w-100 bg-light text-center'>
                <p>@ 2021 . BufferZero . All Rights Reserved| Terms and Condition</p>
        </footer>
     );
}
 
export default Footer;