import React, { Component } from 'react';
// import { NavLink } from 'react-router-dom';
import Common from './common';
import rafale2 from './rafale2.jpg'
const Home = () => {
    return ( 
    <div>
        <Common
         name='Rise your career with ' 
         imgsrc={rafale2} 
         visit='/services'
         btname='Get Started'/>
        </div>
     );
}
 
export default Home;