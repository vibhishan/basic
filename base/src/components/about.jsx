import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import Common from './common';
import apache from './apache.jpg'
const About = () => {
    return ( 
    <div>
       <Common 
            name='Welcome to ' 
            imgsrc={apache} 
            visit='/contact'
            btname='Contact Now'/>
        </div>
     );
}
 
export default About;