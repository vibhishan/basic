import rafale from './rafale.jpg';
import rafale2 from './rafale2.jpg';
import rafale3 from './rafale3.jpeg';
import apache from './apache.jpg';
import romeo from './romeo.jpg';
import chinook from './chinook.jpg';

const Sdata=[
    {
        imgsrc : rafale,
        title : 'rafale_jet',
    },
    {
        imgsrc : apache,
        title : 'apache_helicopter',
    },
    {
        imgsrc : rafale2,
        title : 'rafale2_jet',
    },
    {
        imgsrc : romeo,
        title : 'romeo_helicopter',
    },
    {
        imgsrc : rafale3,
        title : 'rafale3_jet',
    },
    {
        imgsrc : chinook,
        title : 'chinook_helicopter',
    },
    
];
export default Sdata