import './App.css';
import{Route, Switch} from 'react-router-dom'
import Home from './components/home';
import Services from './components/services';
import About from './components/about';
import Contact from './components/contact';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/bootstrap/dist/js/bootstrap.bundle.js';
// import Navbar2 from './components/navbar2';
import NavBar from './components/navBar';
import Footer from './components/footer';

function App() {
  return (
    <div className="">
      {/* <h1 className='Title'>Abhishek</h1> */}
      <NavBar/>
      {/* <Navbar2/> */}
      <Switch>
        <Route path='/services' component={Services}/>
        <Route path='/about'    component={About}/>
        <Route path='/contact'  component={Contact}/>
        <Route path='/'exact    component={Home}/>
      </Switch>
       <Footer/>
    </div>
  );
}

export default App;
